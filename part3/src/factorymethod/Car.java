package src.factorymethod;

public interface Car<T> {
	public T create();
}

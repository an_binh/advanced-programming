# Part 2:
You are required to implement design patterns using an IDE such as Eclipse. Tasks are following:
* Setup of a project with version control. develop a small application that implements a few examples of creational, structural and behavioral design patterns.

* Evaluate the use of design patterns for the given purpose specified. Discuss trade-offs/consequences by applying design patterns.

Note: You must submit a report and full examples code.

# Part 3:
Consider an example of online taxi services (i.e., Grab). The company has a list of customers and drivers.
When a customer requests a cab, then a driver accepts and visits the customer for a ride. Which pattern can be used to implement this scenario? Explain with a UML diagram and also explain the list of classes and methods involved in this pattern. After that, you must use an IDE (Eclipse) to implement your design.
Note: You must submit a report and your code.